import { nanoid } from "nanoid";
import "./App.css";


function App() {

  return (
    <main>
      <div className="home">
        <div className="home-logo">
          <img src={require("./components/tfos_blk.png")} alt="tfos logo" />
        </div>
      <h1>Time for other Stuff</h1>
      <h2>Your sharable, synchronous Kanban Board</h2>
      <a href={"/"+nanoid(8)}> <button>Start a new Board!</button> </a>
    </div>
    </main>
  );
}

export default App;
