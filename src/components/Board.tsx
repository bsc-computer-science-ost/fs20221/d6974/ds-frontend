import { useEffect, useState } from "react";
import Buckets from "./Buckets";
import { board, bucket, item } from "../utils/classes";
import { Link, useParams } from "react-router-dom";
import getRandomID from "../utils/randId";
import { ImSpinner8, ImCloudCheck } from "react-icons/im";


class updateMessage {
  constructor(
    public type: string,
    public boardId: string,
    public boardData: board
  ) {}
}

class getMessage {
  constructor(
    public type: string, 
    public boardId: string
    ) {}
}

const DEV_MODE = false;

const Board = () => {
  const { boardId } = useParams();
  const [boardData, setBoardData] = useState(new board("", "", []));
  const [title, setTitle] = useState(boardData.title);
  
  const logs = DEV_MODE;
  const [socket, setSocket] = useState<WebSocket>();
  const [socketState, setSocketState] = useState("new");

  useEffect(() => {
    initializeSocket();
  }, []);

  const initializeSocket = () => {
    const ws = new WebSocket("ws://localhost:8080");

    logs && console.log("useeffect ran");

    ws.onerror = () => {
      console.log("Websocket error");
      setSocketState("error");
    };

    ws.onclose = () => {
      console.log("Websocket closed");
      setSocketState("error");
      //setTimeout(()=>{ws = new WebSocket("ws://localhost:8080")}, 5000);
    };

    ws.onopen = () => {
      setSocketState("open");
      let request = {
        type: "get",
        boardId: boardId,
      };
      console.log("Websocket open");
      logs &&
        console.log("[Sending WebSocket Request]" + JSON.stringify(request));
      ws.send(JSON.stringify(request));
    };

    ws.onmessage = (message) => {
      //console.log("Message Data: " + message.data);
      //console.log(`[message] received from server: ${json}`);

      try {
        const reply = JSON.parse(message.data);
        if (reply.type === "update" && boardId === reply.boardId) {
          if (JSON.stringify(boardData) !== JSON.stringify(reply.boardData)) {
            logs && console.log("boardupdate, not equal data");
            logs && console.log(reply.boardData);
            logs && console.log(boardData);
            setBoardData(reply.boardData);
            setTitle(reply.boardData.title);
          } else {
            logs && console.log("equalData, board not updated");
          }
        }
      } catch (err) {
        console.error("Error, not JSON");
        console.error(message.data);
      }
    };

    setSocket(ws);
  };

  // reconnect after disconnect from server
  useEffect(() => {
    logs && console.log("useeffect socketstate: "+ socketState);
    if (socketState === "error" && socket !== undefined) {
      socket.close()
      setSocketState("new")
      setTimeout(()=> initializeSocket(), 5000);
    }
  }, [socketState]);

  const logBoardData = () => {
    logs && console.log(boardData);
  };

  const pushUpdateToServer = (newBoardData : board) => {
    const request = new updateMessage("update", boardData._id, newBoardData);
    logs && console.log(request);
    try {
      if (!socket) throw new Error("no socket");
      socket.send(JSON.stringify(request));
    } catch (error) {
      console.error("update failed");
      console.error(error);
    }
  };
  
  const changeBoardTitle = () => {
    const newBoardData = { ...boardData, title: title }
    pushUpdateToServer(newBoardData);
    setBoardData(newBoardData);
  };

  const changeBucket = (bucketId: number, title: string, items: item[]) => {
    const updatedBuckets = boardData.buckets.map((bucket) => {
      if (bucket._id === bucketId) {
        bucket.title = title;
        bucket.items = items;
      }
      return bucket;
    });

    const newBoardData = { ...boardData, buckets: updatedBuckets }
    pushUpdateToServer(newBoardData);
    setBoardData(newBoardData);
  };

  const addBucket = () => {

    const newBoardData = {
      ...boardData,
      buckets: [
        ...boardData.buckets,
        new bucket(getRandomID(), "New Bucket", []),
      ],
    }
    pushUpdateToServer(newBoardData);
    setBoardData(newBoardData);
  };

  const removeBucket = (bucketId: number) => {
    logs && console.log("removeBucket " + bucketId);
    const newBuckets = boardData.buckets.filter(
      (bucket) => bucket._id !== bucketId
    );
    const newBoardData = { ...boardData, buckets: newBuckets }
    pushUpdateToServer(newBoardData);
    setBoardData(newBoardData);
  };

  const removeItemFromAllBuckets = (itemId: number) => {
    logs && console.log("remove from all " + itemId);
    boardData.buckets.map((bucket) => {
      bucket.items = bucket.items.filter((item: item) => item._id !== itemId);
      return bucket;
    });
    pushUpdateToServer(boardData);
  };

  return (
    <div className="App">
      <header className="app-header">
        <section className="header-logo">
          <Link to="/">
            <img src={require("./tfos_whi.png")} alt="tfos logo" />
          </Link>
        </section>
        <section className="header-title">
          <div className="header-title-form">
            <input
              type="text"
              onBlur={changeBoardTitle}
              onChange={(e) => {
                setTitle(e.target.value);
              }}
              placeholder="Set a Board Title"
              value={title}
            />
          </div>
        </section>
        <section className="header-tools">
          {DEV_MODE && (
            <button className="header-tools-btn" onClick={logBoardData}>
              LogBoardData 
            </button>
          )}
          { socketState === "new" && (
            <div className="msg msg-error"><div className="spinner">< ImSpinner8 /></div>&nbsp;Trying to connect...</div>
          )}
          { socketState === "open" && (
            <div className="msg msg-good">< ImCloudCheck />&nbsp;Connection good</div>
          )}
          
        </section>
      </header>

      <main id="main-board">
        <Buckets
          changeBucket={changeBucket}
          removeBucket={removeBucket}
          removeItemFromAllBuckets={removeItemFromAllBuckets}
          data={boardData.buckets}
        />

        <section className="add-bucket">
          <button onClick={addBucket}>+ Add Bucket</button>
        </section>
      </main>
    </div>
  );
};

export default Board;
