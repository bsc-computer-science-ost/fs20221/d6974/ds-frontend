import { nanoid } from "nanoid";
import { DragEvent, useState } from "react";
import { bucket, item } from "../utils/classes";
import { ImBin } from "react-icons/im";
import getRandomID from "../utils/randId";
import BucketItem from "./BucketItem";

type BucketProps = {
  title: string;
  data: bucket;
  changeBucket: Function;
  removeBucket: Function;
  removeItemFromAllBuckets: Function;
  items: [];
};

const Bucket = (props: BucketProps) => {
  const [title, setTitle] = useState(props.data.title);
  const [dropZone, setDropZone] = useState(false)
  

  const addItem = () => {
    props.changeBucket(props.data._id, props.data.title, [
      ...props.data.items,
      new item(getRandomID(), "New Item", "", "", []),
    ]);
  };

  const changeItem = (newItem : item) => {
    const itemId = newItem._id;
    const newItems : item[] = props.data.items.map( (item : item) => {
      if (item._id ===  itemId) {
          item = newItem;    
      }
      return item;
  })
    //console.log(newItems);
    props.changeBucket(
      props.data._id, 
      props.data.title, 
      newItems
    );

  }

  const removeItem = (itemId : number) => {
    console.log("remove " + itemId);
    const newItems = props.data.items.filter((item :item) => item._id !== itemId )
    props.changeBucket(
      props.data._id, 
      props.data.title, 
      newItems
    )
  }
/*
  const handleDragEnter = (e: any) => {
    console.log(e)
    //console.log("enter")
    if (e.target.className === "board-bucket ") {
      console.log("match");
      setDropZone(true);
    }
    e.preventDefault();
    e.stopPropagation();
  };
  const handleDragLeave = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    if (e.target.className === "board-bucket bucket-drop-zone") {
      setDropZone(false);
      console.log(e)
      console.log("leave");
      
    }
  };*/

  const handleDragOver = (e: DragEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
  };
  const handleDrop = (e: DragEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
    setDropZone(false);
    const draggedItem = JSON.parse(e.dataTransfer.getData("text")) as item;
    //console.log(draggedItem);
    if (props.data.items.find((item) => item._id === draggedItem._id)) {
      console.log("item is already in this bucket");
    } else {
      // delete item from other bucket ?
      props.removeItemFromAllBuckets(draggedItem._id)
      props.changeBucket(props.data._id, props.data.title, [
        ...props.data.items,
        draggedItem,
      ]);
    }
  };
    //onDragEnter={e => handleDragEnter(e)}
    //onDragLeave={e => handleDragLeave(e)}
  return (
    <section 
    className={"board-bucket " + (dropZone ? "bucket-drop-zone" : "")}
    onDrop={e => handleDrop(e)}
    onDragOver={e => handleDragOver(e)}
    
      >
      <header className="bucket-title">
        <input
          type="text"
          name="title"
          id="title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          onBlur={() =>
            props.changeBucket(props.data._id, title, props.data.items)
          }
        />
        <button onClick={() => props.removeBucket(props.data._id)}><ImBin /></button>
      </header>
      {props.data.items.length > 0 ? (
        props.data.items.map((item: any) => (
          <BucketItem
            removeItem={removeItem}
            changeItem={changeItem}
            item={item}
            key={nanoid(4)}
          />
        ))
      ) : (
        <div className="bucket-empty">
          <p>This Bucket is empty</p>
        </div>
      )}

      <div className="bucket-add-item">
        <button onClick={addItem}>+ Add Item</button>
      </div>
    </section>
  );
};

export default Bucket;
