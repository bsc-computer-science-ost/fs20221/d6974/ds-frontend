import React, { useState } from "react"
import { ImBin, ImFloppyDisk, ImPencil } from "react-icons/im";
import { item } from "../utils/classes"

type BucketItemProps = {
  item: item
  removeItem: Function
  changeItem: Function
}


const BucketItem = ({item, removeItem, changeItem} : BucketItemProps) => {
  const [editMode, setEditMode] = useState(false)
  const [editItem, setEditItem] = useState(item)

  const toggleEditMode = () => {
    setEditMode(!editMode)
  }

  const handleChange = (event:React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;    
    setEditItem(prevState => ({
        ...prevState,
        [name]: value
    }));
};

  const handleSubmit = (event : any) => {
    event.preventDefault();
    changeItem(editItem)
  }

  const handleDragStart = (e: React.DragEvent<HTMLDivElement>) => {
    //e.preventDefault();
    //e.stopPropagation();
    e.dataTransfer.setData("text/plain", JSON.stringify(item))
  }


  const handleDragEnd = (e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
    e.stopPropagation();
    //console.log("onDragEnd")
    //removeItem(item._id)
  }


  return (
    <div 
      className="bucket-item" 
      draggable={editMode ? false : true}
      onDragStart={e => handleDragStart(e)}
      onDragEnd={e => handleDragEnd(e)}
    >
      {editMode ? (
        <form onSubmit={handleSubmit}>
          <label htmlFor="title">Title: </label>
          <input
            type="text"
            name="title"
            id="title"
            value={editItem.title}
            onChange={(e) => handleChange(e)}
          />
          <label htmlFor="description">Description: </label>
          <input
            type="text"
            name="description"
            value={editItem.description}
            onChange={(e) => handleChange(e)}
          />
          <button type="submit">
            <ImFloppyDisk /> Save
          </button>
        </form>
      ) : (
        <>
          <header>
            <span>{item.title}</span>
            <button onClick={() => removeItem(item._id)}>
              {" "}
              <ImBin />
            </button>
            <button onClick={toggleEditMode}>
              <ImPencil />
            </button>
          </header>
          <main>{item.description}</main>
          {/*<footer>
            <span className="bucket-item-tag">#tag</span>
            <span className="bucket-item-tag">#tag</span>
            <span className="bucket-item-tag">#tag</span>
      </footer>*/}
        </>
      )}
    </div>
  );
}

export default BucketItem