import { nanoid } from "nanoid";
import Bucket from "./Bucket";

const Buckets = (props: any) => {
  return (
    <>
      {props.data.length > 0 ? (
        props.data.map((bucket: any) => (
          <Bucket
            key={nanoid(4)}
            removeItemFromAllBuckets={props.removeItemFromAllBuckets}
            title={bucket.title}
            data={bucket}
            changeBucket={props.changeBucket}
            removeBucket={props.removeBucket}
            items={bucket.items}
          />
        ))
      ) : (
        <div>
        <h1>This Board is empty.</h1>
        <h2>Add a new Bucket by clicking the Button "Add Bucket"</h2>
        </div>
      )}
    </>
  );
};

export default Buckets;
