export class board {
    constructor(
        public title : string, 
        public _id : string,
        public buckets: bucket[]
    ) {}
}

export class bucket {
    constructor ( 
        public _id : number,
        public title : string,
        public items : item[]
    ) {}
}

export class item {
    constructor ( 
        public _id : number,
        public title : string,
        public description: string,
        public color: string,
        public tags: tag[]
    ) {}
}

export class tag {
    constructor ( 
        public _id : number,
        public title : string,
    ) {}
}